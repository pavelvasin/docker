#  Magento 1 Docker to Development

### Apache 2 + PHP 5.5 + MariaDB + Magerun + OPCache

Fork from [Rafael Corra Gomes](https://github.com/rafaelstz/) repo [clean-docker/Magento1](https://github.com/clean-docker/Magento1)

Run in the root directory of multisiting

```
./init
```

---

Please, read file [howto.md](https://bitbucket.org/pavelvasin/docker/src/master/howto.md)

---

- **Web server:** http://localhost/
- **PHPMyAdmin:** http://localhost:8080
- **Local emails:** http://localhost:8025

---

### Features commands

| Commands  | Description  | Options & Examples |
|---|---|---|
| `./init`  | If you didn't use the CURL setup command above, please use this command changing the name of the project.  | `./init MYMAGENTO` |
| `./start`  | If you continuing not using the CURL you can start your container manually  | |
| `./stop`  | Stop your project containers  | |
| `./kill`  | Stop your project containers and remove named volumes  | |
| `./shell`  | Access your container  | `./shell root` | |
| `./magento`  | Use the power of the Magento CLI  | |
| `./n98`  | Use the Magerun commands as you want | |
| `./xdebug`  |  Enable / Disable the XDebug | |

### License

MIT 2017 [Rafael Corra Gomes](https://github.com/rafaelstz/) and contributors.
