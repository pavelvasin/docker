### Докер для мультисайтинга на Ubuntu

- удаляем/останавливаем контейнеры использующие порты
    - 80 / 8080 / 8025	
- в директории C:\www клонируем докер
    - git clone https://pavelvasin@bitbucket.org/pavelvasin/docker.git
- удаляем директорию .git
- запускаем скрипт init
    - ./init
- регистрируем домен в файле hosts на Windows
    - 127.0.0.1 hello.world
- создаем папку проекта hello.world
    - C:\www\docker\src\hello.world
- в папке проекта создаем index-файл
    - C:\www\docker\src\hello.world\index.php
- входим в контейнер docker_apache_1
    - docker exec -it docker_apache_1 bash
- устанавливаем nano
    - apt-get install nano
- создаем/редактируем файл виртуального хоста для hello.world
    - cp /etc/apache2/sites-available/magento.conf /etc/apache2/sites-available/hello.world.conf
    - nano /etc/apache2/sites-available/hello.world.conf  

```
<VirtualHost *:80>
  ServerAdmin admin@yourdomain.com
  DocumentRoot /var/www/html/hello.world/
  ServerName hello.world
  ServerAlias www.hello.world
  <Directory /var/www/html/hello.world/>
	  Options FollowSymLinks
	  AllowOverride All
	  Order allow,deny
	  allow from all
  </Directory>
  ErrorLog /var/log/apache2/magento-error_log
  CustomLog /var/log/apache2/magento-access_log common
</VirtualHost>
```

- активируем домен hello.world
    - a2ensite hello.world
- перезагружаем Apache
    - service apache2 reload
- создаем базу данных hello_world
    - http://localhost:8080/
- переходим в корень домена C:\www\docker\src\hello.world
- загружаем движок Magento в C:\www\docker\src\hello.world
    - git clone https://git.cyberhull.com/training/magento-practice.git
- переносим все файлы из директории magento-practice в корень домена
- создаем папку media
- открываем в браузере http://hello.world/
- на шаге подключения к базе данных используем
    - host: db
    - database: hello_world
    - user: root
    - password: root

